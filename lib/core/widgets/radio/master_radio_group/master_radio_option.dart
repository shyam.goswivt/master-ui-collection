import 'package:flutter/material.dart';

class MasterRadio<T> extends StatelessWidget {
  final T value;
  final T? groupValue;
  final Widget unselectedRadio;
  final Widget selectedRadio;
  final String text;
  final Color borderColor;
  final Color selectedbackgroundColor;
  final double borderRadius;
  final ValueChanged<T?> onChanged;

  const MasterRadio({
    super.key,
    required this.value,
    required this.groupValue,
    this.unselectedRadio = const Icon(Icons.radio_button_unchecked),
    this.selectedRadio = const Icon(Icons.radio_button_checked),
    required this.text,
    this.selectedbackgroundColor = Colors.transparent,
    this.borderColor = Colors.transparent,
    this.borderRadius = 4,
    required this.onChanged,
  });

  Widget _buildLabel() {
    final bool isSelected = value == groupValue;

    return isSelected ? selectedRadio : unselectedRadio;
  }

  Widget _buildText() {
    return Text(
      text,
      style: const TextStyle(color: Colors.black, fontSize: 24),
    );
  }

  @override
  Widget build(BuildContext context) {
    final bool isSelected = value == groupValue;

    return GestureDetector(
      onTap: () => onChanged(value),
      child: Container(
        decoration: BoxDecoration(
            color: isSelected ? selectedbackgroundColor : Colors.transparent,
            borderRadius: BorderRadius.circular(borderRadius),
            border: (isSelected && borderColor != Colors.transparent)
                ? Border.all(color: borderColor, width: 1)
                : Border.all(color: Colors.transparent, width: 0)),
        child: Row(
          children: [
            _buildLabel(),
            const SizedBox(width: 10),
            _buildText(),
          ],
        ),
      ),
    );
  }
}
