import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../masterui.dart';

enum BorderStyle { underline, circular }

class MasterTextField extends StatefulWidget {
  final FormFieldValidator<String>? validator;
  final TextInputType textInputType;
  final EdgeInsets contentPadding;
  final String? hintText;
  final String? labelText;
  final Widget? prefixWidget;
  final Widget? suffixWidget;
  final bool obscureText;
  final bool isDense;
  final int maxLength;
  final int maxLine;
  final Key? key1;
  final bool digitsOnly;
  final Color? labelColor;
  final String prefixText;
  final bool readOnly;
  final TextInputAction textInputAction;
  final ValueChanged<String>? onChanged;
  final TextCapitalization textCapitalization;
  final TextEditingController controller;
  final Color? textColor;
  final Function? onTap;
  final String? initialValue;
  final Function? onSend;
  final Color fillColor;
  final Color focusedBorderColor;
  final Color borderColor;
  final double borderRadius;
  final TextStyle hintStyle;
  final String? title;
  final double titleSpace;

  const MasterTextField({
    super.key,
    this.validator,
    this.obscureText = false,
    this.textCapitalization = TextCapitalization.none,
    this.digitsOnly = false,
    this.borderRadius = 4,
    this.hintStyle = const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
    required this.controller,
    this.focusedBorderColor = Colors.transparent,
    this.textInputType = TextInputType.text,
    this.contentPadding = const EdgeInsets.all(20),
    this.prefixWidget,
    this.titleSpace = spacing8,
    this.title,
    this.suffixWidget,
    this.hintText = '',
    this.textInputAction = TextInputAction.next,
    this.maxLength = 50,
    this.onChanged,
    this.textColor,
    this.prefixText = "",
    this.labelColor,
    this.borderColor = Colors.black12,
    this.labelText,
    this.readOnly = false,
    this.maxLine = 1,
    this.key1,
    this.onTap,
    this.initialValue,
    this.isDense = false,
    this.onSend,
    this.fillColor = const Color(0xffF6F6F6),
  });

  @override
  State<MasterTextField> createState() => _MasterTextFieldState();
}

class _MasterTextFieldState extends State<MasterTextField> {
  late bool isPasswordInVisible;

  @override
  void initState() {
    isPasswordInVisible = widget.obscureText;
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(widget.title ?? ''),
            SizedBox(height: widget.titleSpace),
            TextFormField(
              key: widget.key1,
              readOnly: widget.readOnly,
              cursorColor: widget.textColor,
              validator: widget.validator,
              obscureText: isPasswordInVisible,
              textCapitalization: widget.textCapitalization,
              textInputAction: widget.textInputAction,
              controller: widget.controller,
              onChanged: widget.onChanged,
              initialValue: widget.initialValue,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              maxLines: widget.maxLine,
              onTap: () => widget.onTap,
              onFieldSubmitted: (value) => widget.onSend?.call(value),
              inputFormatters: widget.digitsOnly
                  ? <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly,
                      LengthLimitingTextInputFormatter(widget.maxLength),
                    ]
                  : [],
              keyboardType: widget.textInputType,
              decoration: InputDecoration(
                isDense: widget.isDense,
                border: UnderlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(widget.borderRadius)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(widget.borderRadius)),
                  borderSide: BorderSide(color: widget.borderColor),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(widget.borderRadius)),
                  borderSide: const BorderSide(color: Colors.red),
                ),
                suffixIcon: widget.suffixWidget ??
                    (widget.obscureText
                        ? (isPasswordInVisible
                            ? IconButton(
                                onPressed: () {
                                  setState(() {
                                    isPasswordInVisible = false;
                                  });
                                },
                                icon: const Icon(
                                  Icons.visibility_off,
                                  color: Colors.black,
                                ),
                              )
                            : IconButton(
                                onPressed: () {
                                  setState(() {
                                    isPasswordInVisible = true;
                                  });
                                },
                                icon: const Icon(Icons.visibility),
                              ))
                        : const SizedBox()),
                prefixIcon: widget.prefixWidget,
                filled: true,
                prefixText: widget.prefixText,
                fillColor: widget.fillColor,
                focusedBorder: OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(widget.borderRadius)),
                  borderSide: BorderSide(color: widget.focusedBorderColor),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(widget.borderRadius)),
                  borderSide: BorderSide(color: Colors.grey),
                ),
                errorStyle: const TextStyle(fontSize: 12),
                labelStyle: TextStyle(color: widget.labelColor, fontSize: 16),
                contentPadding: widget.contentPadding,
                labelText: widget.labelText,
                hintText: widget.hintText,
                hintStyle: widget.hintStyle,
              ),
            ),
          ],
        ),
      );
}
