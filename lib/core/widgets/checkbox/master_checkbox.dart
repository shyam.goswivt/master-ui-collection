import 'package:flutter/material.dart';

class MasterCheckbox<T> extends StatelessWidget {
  final T initialValue;
  final T finalValue;
  final ValueChanged<T?> onChanged;
  final Color backgroundColor;
  final Color forgroundColor;
  final bool enableCheckMark;
  final double height, width;

  const MasterCheckbox(
      {super.key,
      required this.initialValue,
      required this.finalValue,
      required this.onChanged,
      this.backgroundColor = Colors.blue,
      this.forgroundColor = Colors.white,
      this.enableCheckMark = true,
      this.height = 20,
      this.width = 20});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onChanged(initialValue),
      child: _customRadioButton,
    );
  }

  Widget get _customRadioButton {
    final isSelected = initialValue == finalValue;
    return Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: isSelected ? backgroundColor : null,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(
            color: isSelected ? backgroundColor : Colors.grey[300]!,
            width: 2,
          ),
        ),
        child: isSelected && enableCheckMark
            ? Icon(
                Icons.check,
                color: forgroundColor,
                size: 15,
              )
            : null);
  }
}
