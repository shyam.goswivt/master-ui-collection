import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class Radioitem {
  String name;
  String cast;

  Radioitem(this.name, this.cast);
}

class RadioView extends StatefulWidget {
  const RadioView({Key? key}) : super(key: key);

  @override
  State<RadioView> createState() => _RadioViewState();
}

class _RadioViewState extends State<RadioView> {
  Radioitem? _groupValue;

  var radioList = [
    Radioitem("Car", "4 wheeker"),
    Radioitem("Bike", "2 Wheeler")
  ];

  ValueChanged<Radioitem?> _valueChangedHandler() {
    return (value) => setState(() => _groupValue = value!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ...radioList.map((e) {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: MasterRadio<Radioitem>(
                value: e,
                groupValue: _groupValue,
                // selectedRadio: const Icon(Icons.circle),
                // unselectedRadio: const Icon(Icons.circle_outlined),
                text: e.name,
                // borderColor: Colors.amber,
                // borderRadius: 40,
                // SelectedbackgroundColor: Colors.blue,
                onChanged: _valueChangedHandler(),
              ),
            );
          }),
          SizedBox(
            height: spacing16,
          ),
          Text("Custom icons"),
          ...radioList.map((e) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: MasterRadio<Radioitem>(
                value: e,
                groupValue: _groupValue,
                selectedRadio: const Icon(Icons.ac_unit),
                unselectedRadio: const Icon(Icons.circle_outlined),
                text: e.name,
                // borderColor: Colors.amber,
                // borderRadius: 40,
                // SelectedbackgroundColor: Colors.blue,
                onChanged: _valueChangedHandler(),
              ),
            );
          }),
          SizedBox(
            height: spacing16,
          ),
          const Text("Radio with background"),
          ...radioList.map((e) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: MasterRadio<Radioitem>(
                value: e,
                groupValue: _groupValue,
                selectedRadio: const Icon(Icons.ac_unit),
                unselectedRadio: const Icon(Icons.circle_outlined),
                text: e.name,
                // borderColor: Colors.amber,
                // borderRadius: 40,
                selectedbackgroundColor: Colors.blue,
                onChanged: _valueChangedHandler(),
              ),
            );
          }),
          SizedBox(
            height: spacing16,
          ),
          const Text("Radio with border and radius"),
          ...radioList.map((e) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: MasterRadio<Radioitem>(
                value: e,
                groupValue: _groupValue,
                text: e.name,
                borderColor: Colors.amber,
                borderRadius: 40,
                onChanged: _valueChangedHandler(),
              ),
            );
          }),
        ]),
      ),
    );
  }
}
