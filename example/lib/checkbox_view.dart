import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class CheckBoxView extends StatefulWidget {
  const CheckBoxView({super.key});

  @override
  State<CheckBoxView> createState() => _CheckBoxViewState();
}

bool finalValue = false;

class _CheckBoxViewState extends State<CheckBoxView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MasterCheckbox<bool>(
                initialValue: false,
                finalValue: finalValue,
                onChanged: (value) {
                  setState(() {
                    finalValue = !finalValue;
                  });
                },
              ),
              const Text('default')
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MasterCheckbox<bool>(
                enableCheckMark: false,
                initialValue: false,
                finalValue: finalValue,
                onChanged: (value) {
                  setState(() {
                    finalValue = !finalValue;
                  });
                },
              ),
              const Text('default with no checkmark')
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MasterCheckbox<bool>(
                height: 50,
                width: 50,
                backgroundColor: Colors.yellow,
                initialValue: false,
                finalValue: finalValue,
                onChanged: (value) {
                  setState(() {
                    finalValue = !finalValue;
                  });
                },
              ),
              const Text('default but big size and background color yellow')
            ],
          ),
        ],
      ),
    );
  }
}
