import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:master_ui_collection/core/widgets/textfield/master_text_field.dart';

class AppTextField extends StatelessWidget {
  const AppTextField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MasterTextField(
            controller: TextEditingController(),
            fillColor: Colors.transparent,
            focusedBorderColor: Colors.amber,
            suffixWidget: const Icon(CupertinoIcons.add_circled_solid),
            prefixWidget: const Icon(CupertinoIcons.add_circled_solid),
            title: "Name",
          ),
          MasterTextField(
            controller: TextEditingController(),
            fillColor: Colors.deepPurpleAccent,
            borderRadius: 50,
            hintText: "with radius",
            focusedBorderColor: Colors.amber,
          ),
          MasterTextField(
            controller: TextEditingController(),
            fillColor: Colors.black12,
            maxLine: 4,
            labelText: "Name",
            labelColor: Colors.black,
            borderColor: Colors.redAccent,
            focusedBorderColor: Colors.green,
          )
        ],
      ),
    );
  }
}
