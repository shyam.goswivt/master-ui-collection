import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: "buttons"),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void _showSnackBar() {
    MasterSnackbar.show(
        externalContext: context,
        content: const Icon(Icons.computer),
        elevation: 20,
        backgroundColor: Colors.yellow);
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.

    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(children: [
          MasterButton(
            buttonText: "test",
            elevation: 10,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            borderColor: Colors.black,
            size: MasterWidgetSize.large,
            onPressed: () {
              setState(() {
                print("hello");
              });
            },
          ),
          MasterButton(
            buttonText: "solid",
            elevation: 0,
            backgroundColor: Colors.blue,
            textColor: Colors.black,
            size: MasterWidgetSize.large,
            onPressed: () {
              setState(() {
                print("hello");
              });
            },
          ),
          MasterButton(
            buttonText: "solid",
            elevation: 4,
            backgroundColor: Colors.blue,
            size: MasterWidgetSize.large,
            radius: 30,
            onPressed: () {
              setState(() {
                print("hello");
              });
            },
          ),
          MasterButton(
            buttonText: "solid",
            elevation: 4,
            backgroundColor: Colors.blue,
            size: MasterWidgetSize.small,
            radius: 30,
            onPressed: () {
              setState(() {
                print("hello");
              });
            },
          ),
          MasterButton(
            elevation: 4,
            iconOnly: true,
            backgroundColor: Colors.blue,
            size: MasterWidgetSize.medium,
            minWidth: 40,
            radius: 30,
            icon: const Icon(Icons.access_alarm),
            onPressed: () {
              setState(() {
                print("hello");
              });
            },
          ),
          MasterButton(
            elevation: 4,
            iconOnly: true,
            backgroundColor: Colors.white,
            size: MasterWidgetSize.mini,
            borderColor: Colors.amber,
            minWidth: 70,
            textColor: Colors.blue,
            buttonText: 'hi',
            icon: const Icon(
              Icons.ac_unit,
              color: Colors.blue,
            ),
            onPressed: () {
              setState(() {
                print("hello");
              });
            },
          ),
          MasterChips(
            onPressed: _showSnackBar,
            label: const Text('Test'),
            backgroundColor: Colors.yellow,
          )
        ]),
      ),
    );
  }
}
